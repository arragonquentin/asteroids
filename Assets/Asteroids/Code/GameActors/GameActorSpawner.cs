using UnityEngine;

namespace QuentinArragon.AsteroidsGame
{
    public class GameActorSpawner : MonoBehaviour
    {
        [SerializeField] private GameActorSpawnStrategy spawnStrategy;
        [SerializeField] private GameActorsPool pool;

        public void DoSpawn()
        {
            Bounds playgroundBounds = Utility.CalculatePlaygroundBounds(Camera.main);
            spawnStrategy.DoSpawn(playgroundBounds, pool);
        }
    }
}
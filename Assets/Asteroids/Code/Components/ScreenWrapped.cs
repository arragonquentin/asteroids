using UnityEngine;

namespace QuentinArragon.AsteroidsGame
{
    public class ScreenWrapped : MonoBehaviour
    {
        private bool isWrappingX;
        private bool isWrappingZ;

        private Bounds playgroundBounds;
        private Bounds meshesBounds;
        /// <summary>
        /// 'radius' correspond to the greater extent of the gameObject meshes bounds.
        /// It's value is used to check if the gameObject is offscreen via the 'IsVisible' property.
        /// </summary>
        private float radius;
        
        public bool IsVisible
        { 
            get
            {
                float x = transform.position.x;
                float z = transform.position.z;
                
                if (Mathf.Abs(x) > Mathf.Abs(playgroundBounds.extents.x) + radius)
                    return false;
                if (Mathf.Abs(z) > Mathf.Abs(playgroundBounds.extents.z) + radius)
                    return false;
                
                return true;
            }
        }
        
        void Awake()
        {
            playgroundBounds = Utility.CalculatePlaygroundBounds(Camera.main);
            meshesBounds = Utility.GetMeshesBounds(gameObject);
            radius = Mathf.Max(meshesBounds.extents.x, meshesBounds.extents.y, meshesBounds.extents.z);
        }

        private void Update()
        {
            ScreenWrap();
        }

        void ScreenWrap()
        {
            if(IsVisible)
            {
                isWrappingX = false;
                isWrappingZ = false;
                return;
            }
            
            if(isWrappingX && isWrappingZ) 
            {
                return;
            }
            
            float x = transform.position.x;
            float z = transform.position.z;
            
            Vector3 newPosition = transform.position;
            
            if(!isWrappingX && Mathf.Abs(x) > Mathf.Abs(playgroundBounds.extents.x))
            {
                newPosition.x = -x;
                isWrappingX = true;
            }
            if(!isWrappingZ && Mathf.Abs(z) > Mathf.Abs(playgroundBounds.extents.z))
            {
                newPosition.z = -z;
                isWrappingZ = true;
            }
            transform.position = newPosition;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.cyan;
            // Gizmos.DrawWireCube(transform.position + meshesBounds.center, meshesBounds.size);
            Gizmos.DrawWireSphere(transform.position, radius);
        }
    }
}
using System;
using System.Collections.Generic;
using Gists;
using JetBrains.Annotations;
using QuentinArragon.AsteroidsGame;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

namespace QuentinArragon.AsteroidsGame
{
    [CreateAssetMenu(fileName = "Random Distribution Strategy", menuName = "Asteroids/Game Actors DoSpawn Strategies/Random Distribution", order = 0)]
    public class RandomDistributionStrategy : GameActorSpawnStrategy
    {
        [SerializeField] private string gameActorTypeId;
        [Header("Spawning")]
        [SerializeField] private int seed;
        [SerializeField] private int count = 10;
        [SerializeField] private float minimumDistance = 1;
        [Header("Movement")] 
        [SerializeField] private float speed = 1;
        
        public override void DoSpawn([NotNull] Bounds playgroundBounds, GameActorsPool pool)
        {
            Random.InitState(seed);
            Vector2 bottomLeft = new Vector2(playgroundBounds.min.x, playgroundBounds.min.z);
            Vector2 topRight = new Vector2(playgroundBounds.max.x, playgroundBounds.max.z);
            
            List<Vector2> posSamples = FastPoissonDiskSampling.Sampling(bottomLeft, topRight, minimumDistance);
            // The spawn position are more evenly distributed when the list is shuffled
            posSamples.Shuffle();
            
            for (int i = 0; i < count; i++)
            {
                if (count >= posSamples.Count)
                {
                    Debug.LogError("Not enough samples. You need to lower the 'count' or the 'minimum distance' value");
                    break;
                }
                Vector3 pos = new Vector3(posSamples[i].x, 0, posSamples[i].y);
                Vector3 rot = new Vector3(0, Random.Range(0,360), 0);
                GameActor gameActor = pool.GetGameActor(gameActorTypeId, pos, Quaternion.Euler(rot));
                ContinuousMovement moveComponent = gameActor.GetComponent<ContinuousMovement>();
                moveComponent.Speed = speed;
                moveComponent.RandomizeDirection();
            }
        }
    }
}
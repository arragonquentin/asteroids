using System;
using UnityEngine;

namespace QuentinArragon.AsteroidsGame
{
    [Serializable]
    public class GameStateMachine
    {
        [SerializeField] private GameStateGameOver gameStateGameOver;
        [SerializeField] private GameStateRunning gameStateRunning;
        
        private IGameState currentState;
        public IGameState CurrentState => currentState;
        public GameStateGameOver GameStateGameOver => gameStateGameOver;
        public GameStateRunning GameStateRunning => gameStateRunning;


        public void Init(Player player)
        {
            gameStateRunning.Init(this, player);
            SetState(gameStateRunning);
        }

        public void SetState(IGameState state)
        {
            currentState?.OnExit();
            currentState = state;
            currentState.OnEnter();
        }
    }
}
using System;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;

namespace QuentinArragon.AsteroidsGame
{
    [RequireComponent(typeof(Rigidbody))]
    public class InputDrivenMovement : MonoBehaviour
    {
        [SerializeField] private Key moveForwardKey;
        [SerializeField] private Key rotateLeftKey;
        [SerializeField] private Key rotateRightKey;
        [SerializeField] float thrust = 1500;
        [SerializeField] private float maxSpeed = 4;
        [SerializeField] float rotationSpeed = 180;

        private Rigidbody body;

        private float forward;
        private float steering;
        
        private void Awake()
        {
            body = GetComponent<Rigidbody>();
        }

        private void Update()
        {
            forward = Keyboard.current[moveForwardKey].isPressed ? 1 : 0;
            float left = Keyboard.current[rotateLeftKey].isPressed ? 1 : 0;
            float right = Keyboard.current[rotateRightKey].isPressed ? 1 : 0;
            steering = right - left;
        }

        private void FixedUpdate()
        {
            if(Mathf.Abs(forward) > 0)
                body.AddForce(transform.forward * forward * thrust * Time.fixedDeltaTime, ForceMode.Acceleration);
            
            if(Mathf.Abs(steering) > 0)
            {
                Quaternion deltaRotation = Quaternion.Euler(new Vector3(0, steering * rotationSpeed, 0) * Time.fixedDeltaTime);
                body.MoveRotation(body.rotation * deltaRotation);
            }

            Vector3 currentVelocity = body.velocity;
            if(currentVelocity.magnitude > maxSpeed)
                body.velocity = Vector3.ClampMagnitude(currentVelocity, maxSpeed);
        }

        public void SetVelocityToZero()
        {
            body.velocity = Vector3.zero;
        }
    }
}
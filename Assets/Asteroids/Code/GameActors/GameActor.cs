using UnityEngine;
using UnityEngine.Serialization;

namespace QuentinArragon.AsteroidsGame
{
    public class GameActor : MonoBehaviour
    {
        [SerializeField] private string actorTypeId;
        [SerializeField] private GameActorStateMachine stateMachine = new GameActorStateMachine();

        public string ActorTypeId => actorTypeId;
        public GameActorStateMachine StateMachine => stateMachine;

        protected virtual void Awake()
        {
            stateMachine.Init(this);
        }
        
        public void ResetGameActor()
        {
            stateMachine.Init(this);
        }        

        public void Kill()
        {
            stateMachine.SetState(stateMachine.DyingState);
        }
    }
}
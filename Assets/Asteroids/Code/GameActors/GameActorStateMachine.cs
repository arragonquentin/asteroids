using System;
using UnityEngine;

namespace QuentinArragon.AsteroidsGame
{
    [Serializable]
    public class GameActorStateMachine
    {
        [SerializeField] private InitState initState;
        [SerializeField] private IdleState idleState;
        [SerializeField] private DyingState dyingState;
        [SerializeField] private DeadState deadState;

        private AGameActorState currentState;
        
        public InitState InitState => initState;
        public IdleState IdleState => idleState;
        public DyingState DyingState => dyingState;
        public DeadState DeadState => deadState;
        public AGameActorState CurrentState => currentState;

        /// <summary>
        /// Init the gameActor state machine that handles its life cycle.
        /// Remove all states events listeners before the gameActor start a new life. 
        /// </summary>
        public void Init(GameActor gameActor)
        {
            initState.Init(this, gameActor);
            idleState.Init(this, gameActor);
            dyingState.Init(this, gameActor);
            deadState.Init(this, gameActor);
            SetState(InitState);
        }

        public void SetState(AGameActorState state)
        {
            // Debug.Log("GameActorStateMachine - Set state : " + state);
            if(currentState != null)
                currentState.Exit();
            if(state == currentState)
                return;
            currentState = state;
            currentState.Enter();
        }
    }
}
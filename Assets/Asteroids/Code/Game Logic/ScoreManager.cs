using System;
using UnityEngine;

namespace QuentinArragon.AsteroidsGame
{
    /// <summary>
    /// When a game actor dies, the ScoreManager look if it has a ScoreAMount attach to it. If it is, the score amount value is added
    /// to the player score.
    /// </summary>
    public class ScoreManager : MonoBehaviour
    {
        [SerializeField] private int currentScore;
        [SerializeField] private GameActorsCollection activeGameActorsCollection;

        public event Action<int> OnScoreChanged; 
        
        private void Awake()
        {
            currentScore = 0;
        }
        
        private void Start()
        {
            OnScoreChanged?.Invoke(currentScore);
        }

        private void OnEnable()
        {
            activeGameActorsCollection.OnAdd += OnGameActorAddedToGame;
        }

        private void OnDisable()
        {
            activeGameActorsCollection.OnAdd -= OnGameActorAddedToGame;
        }

        void OnGameActorAddedToGame(GameActor gameActor)
        {
            gameActor.StateMachine.DyingState.onEnter.AddListener(() => OnGameActorIsDying(gameActor));
        }

        private void OnGameActorIsDying(GameActor gameActor)
        {
            ScoreAmount scoreAmount = gameActor.GetComponent<ScoreAmount>();
            if(scoreAmount == null)
                return;
            currentScore += scoreAmount.Value;
            OnScoreChanged?.Invoke(currentScore);
        }
    }
}
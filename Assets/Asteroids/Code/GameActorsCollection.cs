using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuentinArragon.AsteroidsGame
{
    [CreateAssetMenu(menuName = "Asteroids/Game Actors Collection", fileName = "Game Actors Collection")]
    public class GameActorsCollection : ScriptableObject, IEnumerable<GameActor>
    {
        [SerializeField] private List<GameActor> list = new List<GameActor>();

        public int Count => list.Count;

        public event Action<GameActor> OnAdd;
        public event Action<GameActor> OnRemove;

        public GameActor this[int index]
        {
            get => list[index];
        }
        
        public void Add(GameActor gameActor)
        {
            // Debug.Log(name + " - Add : " + gameActor);
            list.Add(gameActor);
            OnAdd?.Invoke(gameActor);
        }

        public void Remove(GameActor gameActor)
        {
            // Debug.Log(name + " - Remove : " + gameActor);
            OnRemove?.Invoke(gameActor);
            list.Remove(gameActor);
        }

        public void Clear()
        {
            list.Clear();
        }

        public IEnumerator<GameActor> GetEnumerator()
        {
            foreach (GameActor gameActor in list)
                yield return gameActor;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
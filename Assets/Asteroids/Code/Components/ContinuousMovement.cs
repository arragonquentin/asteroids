using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

namespace QuentinArragon.AsteroidsGame
{
    [RequireComponent(typeof(Rigidbody))]
    public class ContinuousMovement : MonoBehaviour
    {
        [SerializeField] float speed = 1;
        [SerializeField] private bool randomizeDirectionEveryNTime;
        [SerializeField] private float timeBetweenDirectionRandomization = 2;
        private Vector3 direction;
        private Rigidbody body;

        public float Speed
        {
            get => speed;
            set => speed = value;
        }

        public Vector3 Direction
        {
            get => direction;
            set => direction = value.normalized;
        }


        private void Awake()
        {
            body = GetComponent<Rigidbody>();
            direction = transform.forward;
        }

        private void OnEnable()
        {
            StopAllCoroutines();
            if (randomizeDirectionEveryNTime)
                StartCoroutine(RandomizeDirectionAfterDelay());
        }

        IEnumerator RandomizeDirectionAfterDelay()
        {
            yield return new WaitForSeconds(timeBetweenDirectionRandomization);
            RandomizeDirection();
            StartCoroutine(RandomizeDirectionAfterDelay());
        }

        private void Update()
        {
            body.velocity = direction * Speed;
        }

        public void SetVelocityToZero()
        {
            body.velocity = Vector3.zero;
        }

        public void RandomizeDirection()
        {
            direction = new Vector3(Random.Range(-1f,1f), 0,Random.Range(-1f,1f)).normalized;
        }
    }
}

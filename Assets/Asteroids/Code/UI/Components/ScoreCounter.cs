using TMPro;
using UnityEngine;

namespace QuentinArragon.AsteroidsGame
{
    public class ScoreCounter : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI text;
        [SerializeField] private ScoreManager scoreManager;

        private readonly string format = "{0}";
        
        private void Awake()
        {
            scoreManager.OnScoreChanged += OnScoreChanged;
        }

        private void OnScoreChanged(int value)
        {
            text.SetText(format, value);
        }
    }
}
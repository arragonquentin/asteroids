using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

namespace QuentinArragon.AsteroidsGame
{
    /// <summary>
    /// When a game actor with a Sizeable component  dies, spawn one or more game actor at its position.
    /// The size of the new game actors is decreased by 1.
    /// </summary>
    public class RocksDeathHandler : MonoBehaviour
    {
        [SerializeField] private GameActorsPool pool;
        [SerializeField] private string gameActorTypeId;
        [SerializeField] private int count = 2;
        [SerializeField] private GameActorsCollection gameActorsCollection;
        
        
        private void OnEnable()
        {
            gameActorsCollection.OnAdd += OnAddGameActor;
        }

        private void OnDisable()
        {
            gameActorsCollection.OnAdd -= OnAddGameActor;
        }

        private void OnAddGameActor(GameActor gameActor)
        {
            if(!string.Equals(gameActor.ActorTypeId, gameActorTypeId))
                return;
            gameActor.StateMachine.DyingState.onEnter.AddListener(() => OnGameActorIsDying(gameActor));
        }
        
        private void OnGameActorIsDying(GameActor gameActor)
        {
            if(!string.Equals(gameActor.ActorTypeId, gameActorTypeId))
                return;
            if(!gameActor.GetComponent<Sizeable>())
                return;
            int sizeOnKill = gameActor.GetComponent<Sizeable>().Size;
            if(sizeOnKill == 1)
                return; 

            Vector3 eulerAngles = Vector3.zero;
            for (int i = 0; i < count; i++)
            {
                // We want to ensure that the new direction is sufficiently different from the last one
                eulerAngles = new Vector3(0, eulerAngles.y + Random.Range(45,360-45), 0);
                Quaternion rot = Quaternion.Euler(eulerAngles);
                GameActor newObject = pool.GetGameActor(gameActorTypeId, gameActor.transform.position, rot);
                newObject.GetComponent<ContinuousMovement>().RandomizeDirection();
                Sizeable newSizeable = newObject.GetComponent<Sizeable>();
                newSizeable.Size = sizeOnKill - 1;
            }
        }
    }
}
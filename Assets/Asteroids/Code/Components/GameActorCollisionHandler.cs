using UnityEngine;

namespace QuentinArragon.AsteroidsGame
{
    public class GameActorCollisionHandler : MonoBehaviour
    {
        [SerializeField] private Behaviour behaviour;
        [SerializeField] private string[] collidedGameActorIdContains;

        public enum Behaviour
        {
            DestroyThisOnly,
            DestroyOtherOnly,
            DestroyBoth
        }

        private void OnTriggerEnter(Collider other)
        {
            // Debug.Log(gameObject.name + " has collided with " + other.gameObject.name);
            
            GameActor otherGameActor = other.gameObject.GetComponent<GameActor>();
            GameActor thisGameActor = this.gameObject.GetComponent<GameActor>();

            if(thisGameActor == null || otherGameActor == null)
                return;
            
            // Check if the collided gameObject name is contained in an array entry
            bool doesContains = false;
            foreach (string s in collidedGameActorIdContains)
                if (otherGameActor.ActorTypeId.Contains(s))
                    doesContains = true;
            if(!doesContains)
                return;


            switch (behaviour)
            {
                case Behaviour.DestroyThisOnly:
                    thisGameActor.Kill();
                    break;
                case Behaviour.DestroyOtherOnly:
                    otherGameActor.Kill();
                    break;
                case Behaviour.DestroyBoth:
                    otherGameActor.Kill();
                    thisGameActor.Kill();
                    break;
            }
        }
    }
}
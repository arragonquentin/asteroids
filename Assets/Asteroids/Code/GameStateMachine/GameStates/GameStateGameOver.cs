using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace QuentinArragon.AsteroidsGame
{
    /// <summary>
    /// Trigger an event after a delay.
    /// </summary>
    public class GameStateGameOver : MonoBehaviour, IGameState
    {
        [SerializeField] UnityEvent onEnter = new UnityEvent();
        [SerializeField] private float duration = 1;
        [SerializeField] UnityEvent onGameOverEnd = new UnityEvent();

        
        public void OnEnter()
        {
            Debug.Log("Enter state : GAME OVER");
            onEnter.Invoke();
            StartCoroutine(WaitForGameOverEnd());
        }

        IEnumerator WaitForGameOverEnd()
        {
            yield return new WaitForSeconds(duration);
            onGameOverEnd?.Invoke();
        }

        public void OnExit()
        {
            
        }
    }
}
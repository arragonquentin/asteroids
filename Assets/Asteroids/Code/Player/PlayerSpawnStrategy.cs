using System;
using UnityEngine;

namespace QuentinArragon.AsteroidsGame
{
    [CreateAssetMenu(fileName = "Player DoSpawn Strategy", menuName = "Asteroids/Player DoSpawn Strategy", order = 0)]
    [Serializable]
    public class PlayerSpawnStrategy : ScriptableObject
    {
        [SerializeField] private Player prefab;
        [SerializeField] private PlayerSpawnParameters parameters;

        private Player player;

        
        public PlayerSpawnStrategy(Player prefab, PlayerSpawnParameters parameters)
        {
            this.prefab = prefab;
            this.parameters = parameters;
        }
        
        public Player InitPlayer(Bounds playgroundBounds)
        {
            Vector3 pos = playgroundBounds.center;
            Quaternion rot = Quaternion.Euler(new Vector3(0, parameters.rotationY, 0));
            if(player == null)
            {
                player = MonoBehaviour.Instantiate(prefab, pos, rot);
                player.ResetGameActor();
            }
            else
            {
                player.transform.position = pos;
                player.transform.rotation = rot;
                player.ResetGameActor();
            }
            player.LivesCount = parameters.livesCount;
            return player;
        }
    }
}
using UnityEngine;

namespace QuentinArragon.AsteroidsGame
{
    /// <summary>
    /// This component bind the transform local scale to a discrete value representing its size.
    /// </summary>
    public class Sizeable : MonoBehaviour
    {
        [SerializeField, Range(1,10)] private int size = 1;
        [SerializeField, Range(0.05f, 10)] private float visualObjectScaleFactor;

        public int Size
        {
            get => size;
            set
            {
                size = value;
                ApplySize();
            }
        }

        void ApplySize()
        {
            Vector3 scale = Vector3.one * (float) size * visualObjectScaleFactor;
            transform.localScale = scale;
        }

        private void OnValidate()
        {
            if (size < 1)
                size = 1;
            ApplySize();
        }
    }
}
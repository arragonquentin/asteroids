using System;

namespace QuentinArragon.AsteroidsGame
{
    [Serializable]
    public struct PlayerSpawnParameters
    {
        public float rotationY;
        public int livesCount;

        public PlayerSpawnParameters(float rotationY, int livesCount = 5)
        {
            this.rotationY = rotationY;
            this.livesCount = livesCount;
        }
    }
}
using System;
using UnityEngine;

namespace QuentinArragon.AsteroidsGame
{
    public abstract class GameActorSpawnStrategy : ScriptableObject
    {
        public virtual void DoSpawn(Bounds playgroundBounds, GameActorsPool pool)
        {
            throw new NotImplementedException();
        }
    }
}
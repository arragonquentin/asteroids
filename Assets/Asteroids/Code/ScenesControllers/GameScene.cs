using UnityEngine;

namespace QuentinArragon.AsteroidsGame
{
    /// <summary>
    /// Game scene controller.
    /// Hold the game state machine.
    /// Spawn the NPCs at start.
    /// </summary>
    public class GameScene : MonoBehaviour
    {
        [SerializeField] private new Camera camera;
        [SerializeField] private GameStateMachine stateMachine = new GameStateMachine();
        [SerializeField] private GameActorsPool gameActorsPool;
        [SerializeField] private GameActorSpawnStrategy npcSpawnStrategy;
        [SerializeField] private PlayerSpawnStrategy playerSpawnStrategy;
        [SerializeField] private GameUI ui;
        private Player player;
        
        private void Start()
        {
            Application.targetFrameRate = 60;
            Bounds playgroundBounds = Utility.CalculatePlaygroundBounds(camera);
            player = playerSpawnStrategy.InitPlayer(playgroundBounds);
            npcSpawnStrategy.DoSpawn(playgroundBounds, gameActorsPool);
            stateMachine.Init(player);
            ui.Init(player);
        }

        private void OnDrawGizmos()
        {
            if (camera != null)
            {
                Gizmos.color = Color.yellow;
                Bounds bounds = Utility.CalculatePlaygroundBounds(camera);
                Gizmos.DrawWireCube(bounds.center, bounds.size);
            }
        }
    }
}
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

namespace QuentinArragon.AsteroidsGame
{
    public class FlyingSaucerSpawner : MonoBehaviour
    {
        [SerializeField] private GameActorsPool pool;
        [SerializeField] private string gameActorTypeId;
        [SerializeField] private float spawnSafeAreaRadius = 3;
        [SerializeField] private float timeBetweenSpawns = 20;

        private Bounds playgroundBounds;
        private WaitForSeconds waiter;
        
        
        private void Awake()
        {
            playgroundBounds = Utility.CalculatePlaygroundBounds(Camera.main);
            waiter = new WaitForSeconds(timeBetweenSpawns);
        }

        private void Start()
        {
            StartCoroutine(SpawnCoroutine());
        }

        IEnumerator SpawnCoroutine()
        {
            yield return waiter;

            // Decide if the game actor is spawned on the left or on the right side of the screen
            Vector3 spawnPosLeft = playgroundBounds.center + Vector3.right * playgroundBounds.extents.x * -1;
            Vector3 spawnPosRight = playgroundBounds.center + Vector3.right * playgroundBounds.extents.x;

            float side = 0;
            bool sideAttributed = false;

            // The first spawn area to be clear is the one chosen for the spawn 
            while (!sideAttributed)
            {
                if (!Utility.DoesGameActorsExistsInSphere(spawnPosLeft, spawnSafeAreaRadius))
                {
                    side = -1;
                    sideAttributed = true;
                }
                if (!Utility.DoesGameActorsExistsInSphere(spawnPosRight, spawnSafeAreaRadius))
                {
                    side = 1;
                    sideAttributed = true;
                }
                yield return null;
            }

            // Instantiate the game actor
            Vector3 spawnPos = playgroundBounds.center + Vector3.right * playgroundBounds.extents.x * side;
            GameActor gameActor = pool.GetGameActor(gameActorTypeId, spawnPos);
            
            // Give the game actor its direction
            Vector3 moveDirection = new Vector3(-side, 0,Random.Range(-0.5f, 0.5f));
            gameActor.GetComponent<ContinuousMovement>().Direction = moveDirection;
            
            // Invoke the spawn coroutine again when the flying saucer die
            gameActor.StateMachine.DyingState.onEnter.AddListener(OnFlyingSaucerDie);
        }

        private void OnFlyingSaucerDie()
        {
            StopAllCoroutines();
            StartCoroutine(SpawnCoroutine());
        }

        private void OnDrawGizmos()
        {
            playgroundBounds = Utility.CalculatePlaygroundBounds(Camera.main);
            Gizmos.color = Color.grey;
            Vector3 posLeft = playgroundBounds.center - Vector3.right * playgroundBounds.extents.x;
            Vector3 posRight = playgroundBounds.center + Vector3.right * playgroundBounds.extents.x;
            Gizmos.DrawWireSphere(posLeft, spawnSafeAreaRadius);
            Gizmos.DrawWireSphere(posRight, spawnSafeAreaRadius);
            #if UNITY_EDITOR
            UnityEditor.Handles.Label(posLeft,"Flying saucer\nspawn safe zone");
            UnityEditor.Handles.Label(posRight,"Flying saucer\nspawn safe zone");
            #endif
        }
    }
}

using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace QuentinArragon.AsteroidsGame
{
    /// <summary>
    /// Base class for the game actors states
    /// </summary>
    public abstract class AGameActorState : MonoBehaviour
    {
        public UnityEvent onEnter = new UnityEvent();
        public UnityEvent onExit = new UnityEvent();

        private GameActorStateMachine stateMachine;
        private GameActor gameActor;

        protected GameActorStateMachine StateMachine => stateMachine;
        protected GameActor GameActor => gameActor;


        public virtual void Init(GameActorStateMachine stateMachine, GameActor gameActor)
        {
            this.stateMachine = stateMachine;
            this.gameActor = gameActor;
            onEnter.RemoveAllListeners();
            onExit.RemoveAllListeners();
        }
        
        public void Enter()
        {
            onEnter?.Invoke();
            StartCoroutine(OnEnter());
        }

        protected virtual IEnumerator OnEnter()
        {
            yield break;
        }

        public void Exit()
        {
            onExit?.Invoke();
            StartCoroutine(OnExit());
        }
        
        protected virtual IEnumerator OnExit()
        {
            yield break;
        }
    }
}
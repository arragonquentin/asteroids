using UnityEngine;

namespace QuentinArragon.AsteroidsGame
{
    public static class Utility
    {
        public static Bounds CalculatePlaygroundBounds(Camera camera)
        {
            Vector3 bottomLeft = camera.ViewportToWorldPoint(new Vector3(0, 0, camera.transform.position.y));
            Vector3 topRight = camera.ViewportToWorldPoint(new Vector3(1, 1, camera.transform.position.y));
            float width = topRight.x - bottomLeft.x;
            float depth = topRight.z - bottomLeft.z;
            return new Bounds(Vector3.zero, new Vector3(width, 0, depth));
        }

        public static Bounds GetMeshesBounds(GameObject target)
        {
            Bounds combinedBounds = new Bounds();
            MeshFilter[] meshFilters = target.GetComponentsInChildren<MeshFilter>();
            foreach (MeshFilter meshFilter in meshFilters)
            {
                Bounds meshBounds = meshFilter.mesh.bounds;
                meshBounds.size = new Vector3(meshBounds.size.x * meshFilter.transform.localScale.x, 
                    meshBounds.size.y * meshFilter.transform.localScale.y,
                    meshBounds.size.z * meshFilter.transform.localScale.z);
                combinedBounds.Encapsulate(meshBounds);
            }
            return combinedBounds;
        }

        public static bool DoesGameActorsExistsInSphere(Vector3 center, float radius, int layer = Physics.DefaultRaycastLayers)
        {
            return Physics.CheckSphere(center, radius, layer, QueryTriggerInteraction.Collide);
        }
    }
}
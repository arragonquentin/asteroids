namespace QuentinArragon.AsteroidsGame
{
    public interface IGameState
    {
        public void OnEnter();
        public void OnExit();
    }
}
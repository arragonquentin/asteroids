using System;
using System.Collections.Generic;
using UnityEngine;

namespace QuentinArragon.AsteroidsGame
{
    public class GameActorsPool : MonoBehaviour
    {
        [SerializeField] List<GameActor> prefabs = new List<GameActor>();
        [SerializeField] private GameActorsCollection activeGameActorsCollection;

        /// <summary>
        /// Key : id, value : list of game actors
        /// </summary>
        Dictionary<string, List<GameActor>> pool = new Dictionary<string, List<GameActor>>();


        private void Awake()
        {
            activeGameActorsCollection.Clear();
            // Initialize dictionary
            foreach (GameActor entry in prefabs)
                if(!pool.ContainsKey(entry.ActorTypeId))
                    pool.Add(entry.ActorTypeId, new List<GameActor>());
        }

        /// <summary>
        /// If the required gameActor exists in the pool, we reset it and return it.
        /// Otherwise we create a new instance of the gameActor.
        /// </summary>
        public GameActor GetGameActor(string id, Vector3 position = new Vector3(), Quaternion rotation = new Quaternion())
        {
            if (!pool.ContainsKey(id))
                throw new Exception($"No registered prefab has the id '{id}'");

            if(pool[id].Count > 0)
            {
                int index = pool[id].Count - 1;
                GameActor gameActor = pool[id][index];
                pool[id].RemoveAt(index);
                gameActor.transform.position = position;
                gameActor.transform.rotation = rotation;
                gameActor.ResetGameActor();
                activeGameActorsCollection.Add(gameActor);
                // The game actor states events are cleared when the ResetGameActor() is called
                gameActor.StateMachine.DeadState.onEnter.AddListener(() => DismissGameActor(gameActor));
                return gameActor;
            }
            else
            {
                GameActor prefab = GetPrefab(id);
                GameActor gameActor = Instantiate(prefab, position, rotation);
                gameActor.ResetGameActor();
                gameActor.StateMachine.DeadState.onEnter.AddListener(() => DismissGameActor(gameActor)); 
                activeGameActorsCollection.Add(gameActor);
                return gameActor;
            }
        }

        /// <summary>
        /// We remove the gameActor from the gameActors collection, and add it to the pool.
        /// </summary>
        public void DismissGameActor(GameActor gameActor)
        {
            if(gameActor == null)
                throw new Exception($"Trying to return a null object inside the pool.");
            // if(pool[gameActor.ActorTypeId].Contains(gameActor))
            // {
            //     Debug.LogError($"Pool #{gameActor.ActorTypeId} already contains this game actor.");
            //     return;
            // }
            activeGameActorsCollection.Remove(gameActor);
            pool[gameActor.ActorTypeId].Add(gameActor);
        }
        
        private GameActor GetPrefab(string gameActorId)
        {
            foreach (GameActor entry in prefabs)
                if (string.Equals(gameActorId, entry.ActorTypeId))
                    return entry;
            return null;
        }
    }
}
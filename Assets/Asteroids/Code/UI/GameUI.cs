using UnityEngine;

namespace QuentinArragon.AsteroidsGame
{
    public class GameUI : MonoBehaviour
    {
        [SerializeField] private LivesCounter livesCounter;

        public void Init(Player player)
        {
            livesCounter.Init(player);
        }
    }
}

using System.Collections;

namespace QuentinArragon.AsteroidsGame
{
    public class InitState : AGameActorState
    {
        protected override IEnumerator OnEnter()
        {
            StateMachine.SetState(StateMachine.IdleState);
            yield break;
        }
    }
}
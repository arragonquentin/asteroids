using UnityEngine;

namespace QuentinArragon.AsteroidsGame
{
    public class GizmosWireSphere : MonoBehaviour
    {
        [SerializeField] private float radius = 0.1f;
        [SerializeField] private Color color;

        private void OnDrawGizmos()
        {
            Gizmos.color = color;
            Gizmos.DrawWireSphere(transform.position, radius);
        }
    }
}
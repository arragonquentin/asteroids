using System;
using System.Collections;
using UnityEngine;

namespace QuentinArragon.AsteroidsGame
{
    /// <summary>
    /// Handles the player spawn.
    /// Manage the player lives count. When the lives count reach zero, the 'GameOver' state is called.
    /// </summary>
    public class GameStateRunning : MonoBehaviour, IGameState
    {
        [SerializeField] private float playerSpawnSafeZoneRadius = 1;
        private GameStateMachine stateMachine;
        private Player player;
        private Vector3 playerPosAtStart;
        private Quaternion playerRotAtStart;
        
        private GameStateMachine StateMachine => stateMachine;
        
        
        public void Init(GameStateMachine stateMachine, Player player)
        {
            this.stateMachine = stateMachine;
            this.player = player;
        }

        public void OnEnter()
        {
            Debug.Log("Enter game state : RUNNING");
            player.StateMachine.DyingState.onEnter.AddListener(OnPlayerIsDying);
            player.StateMachine.DeadState.onEnter.AddListener(() => StartCoroutine(OnPlayerIsDeadCoroutine()));
            playerPosAtStart = player.transform.position;
            playerRotAtStart = player.transform.rotation;
        }

        private void OnPlayerIsDying()
        {
            player.LivesCount--;
        }

        IEnumerator OnPlayerIsDeadCoroutine()
        {
            if(player.LivesCount == 0)
                StateMachine.SetState(StateMachine.GameStateGameOver);
            else
            {
                while (Utility.DoesGameActorsExistsInSphere(playerPosAtStart, playerSpawnSafeZoneRadius))
                    yield return null;

                player.transform.position = playerPosAtStart;
                player.transform.rotation = playerRotAtStart;
                player.ResetGameActor();
                player.StateMachine.DyingState.onEnter.AddListener(OnPlayerIsDying);
                player.StateMachine.DeadState.onEnter.AddListener(() => StartCoroutine(OnPlayerIsDeadCoroutine()));
            }
        }

      

        public void OnExit()
        {
        }

        private void OnDrawGizmos()
        {
            Bounds playgroundBounds = Utility.CalculatePlaygroundBounds(Camera.main);
            Gizmos.color = Color.white;
            Gizmos.DrawWireSphere(playgroundBounds.center, playerSpawnSafeZoneRadius);
            
#if UNITY_EDITOR
            UnityEditor.Handles.Label(playgroundBounds.center,"Player spawn\nsafe zone");
            UnityEditor.Handles.Label(playgroundBounds.center + Vector3.forward * (playgroundBounds.extents.z + 1),"Playground bounds");
#endif

        }
    }
}
using System.Collections;
using UnityEngine;

namespace QuentinArragon.AsteroidsGame
{
    public class DyingState : AGameActorState
    {
        [SerializeField] private float killEffectDuration = 1.5f;

        private WaitForSeconds waiter;

        private void Awake()
        {
            waiter = new WaitForSeconds(killEffectDuration);
        }

        protected override IEnumerator OnEnter()
        {
            yield return waiter;
            StateMachine.SetState(StateMachine.DeadState);
        }
    }
}
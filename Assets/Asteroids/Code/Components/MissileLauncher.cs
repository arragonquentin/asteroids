using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;

namespace QuentinArragon.AsteroidsGame
{
    public class MissileLauncher : MonoBehaviour
    {
        [SerializeField] private GameActorsPool pool;
        [SerializeField] private string missileGameActorId;
        [SerializeField] private Key launchMissileKey = Key.Space;
        [SerializeField] private Vector3 missileCreationLocalPosition;
        [SerializeField] private float launchesTimeInterval = .3f;

        private float timeAtLastLaunched;
        private float TimeSinceLastLaunched => Time.time - timeAtLastLaunched;
        
        private void Update()
        {
            if (Keyboard.current[launchMissileKey].wasPressedThisFrame
            && TimeSinceLastLaunched > launchesTimeInterval)
            {
                LaunchMissile();
                timeAtLastLaunched = Time.time;
            }
        }
        
        public void LaunchMissile()
        {
            Quaternion rot = Quaternion.LookRotation(transform.forward);
            GameActor missile = pool.GetGameActor(missileGameActorId, transform.TransformPoint(missileCreationLocalPosition), rot);
            missile.GetComponent<ContinuousMovement>().Direction = transform.forward;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(transform.TransformPoint(missileCreationLocalPosition), 0.05f);
        }
    }
}
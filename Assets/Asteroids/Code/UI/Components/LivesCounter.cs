using System.Collections.Generic;
using UnityEngine;

namespace QuentinArragon.AsteroidsGame
{
    public class LivesCounter : MonoBehaviour
    {
        [SerializeField] private RectTransform playerIconTemplate;
        [SerializeField] private float spaces = 10;
        List<RectTransform> playerIcons = new List<RectTransform>();
        
        public void Init(Player player)
        {
            Refresh(player.LivesCount);
            player.OnLivesCountChanged += Refresh;
        }

        void Refresh(int value)
        {
            playerIconTemplate.gameObject.SetActive(false);
            
            if (value > playerIcons.Count)
                CreatePlayerIcons(value);
            
            for(int i = playerIcons.Count - 1; i >= 0; i--)
                playerIcons[i].gameObject.SetActive(i < value);
        }

        void CreatePlayerIcons(int count)
        {
            for (int i = 0; i < count; i++)
            {
                if(i <= playerIcons.Count - 1)
                    continue;
                RectTransform newIcon = Instantiate(playerIconTemplate, transform);
                float xPos = playerIconTemplate.rect.width * i + spaces * i;
                newIcon.transform.localPosition = new Vector3(-xPos, 0, 0);
                newIcon.gameObject.SetActive(true);
                playerIcons.Add(newIcon);
            }
        }
    }
}
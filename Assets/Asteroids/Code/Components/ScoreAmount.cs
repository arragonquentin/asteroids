using UnityEngine;

namespace QuentinArragon.AsteroidsGame
{
    /// <summary>
    /// A component to assign to gameActor to define their score value.
    /// </summary>
    /// <seealso cref="ScoreManager"/>
    public class ScoreAmount : MonoBehaviour
    {
        [SerializeField] private int value;

        public int Value => value;
    }
}
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

namespace QuentinArragon.AsteroidsGame
{
    public class TitleScreenScene : MonoBehaviour
    {
        [SerializeField] private new Camera camera;
        [SerializeField] private GameActorsPool gameActorsPool;
        [SerializeField] private GameActorsCollection npcCollection;
        [SerializeField] private GameActorSpawnStrategy npcSpawnStrategy;
        [SerializeField] private Key startGameKey = Key.Enter;
        [SerializeField] UnityEvent onReceiveStartInput = new UnityEvent();
        
        
        private void Start()
        {
            npcCollection.Clear();
            Bounds playgroundBounds = Utility.CalculatePlaygroundBounds(Camera.main);
            npcSpawnStrategy.DoSpawn(playgroundBounds, gameActorsPool);
        }
        
        private void Update()
        {
            if (Keyboard.current[startGameKey].wasPressedThisFrame)
                onReceiveStartInput?.Invoke();
        }
        
        private void OnDrawGizmos()
        {
            if (camera != null)
            {
                Gizmos.color = Color.yellow;
                Bounds bounds = Utility.CalculatePlaygroundBounds(camera);
                Gizmos.DrawWireCube(bounds.center, bounds.size);
            }
        }
    }
}
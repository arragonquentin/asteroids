using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace QuentinArragon.AsteroidsGame
{
    public class Timer : MonoBehaviour
    {
        [SerializeField] private float duration = 1;
        [SerializeField] UnityEvent onTimerEnd = new UnityEvent();

        private WaitForSeconds waiter;
        
        private void Awake()
        {
            waiter = new WaitForSeconds(duration);
        }

        public void StartTimer()
        {
            StopAndResetTimer();
            StartCoroutine(StartTimerCoroutine());
        }

        IEnumerator StartTimerCoroutine()
        {
            yield return waiter;
            onTimerEnd.Invoke();
        }
        
        public void StopAndResetTimer()
        {
            StopAllCoroutines();
        }
    }
}
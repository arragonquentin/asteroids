using System;
using UnityEngine;

namespace QuentinArragon.AsteroidsGame
{
    /// <summary>
    /// The player is a game actor with a lives count.
    /// </summary>
    [RequireComponent(typeof(Rigidbody))]
    public class Player : GameActor
    {
        private int livesCount;
        public event Action<int> OnLivesCountChanged;

        public int LivesCount
        {
            get => livesCount;
            set
            {
                if(livesCount != value)
                    OnLivesCountChanged?.Invoke(value);
                livesCount = value;
            }
        }
    }
}

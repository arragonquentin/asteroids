using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

namespace QuentinArragon.AsteroidsGame
{
    /// <summary>
    /// The missile spawn position draw a circle around the transform position across time.
    /// 't' define which point of the circle is used. It's value is continuously incremented in the Update() method.
    /// </summary>
    public class MissileAutoLauncher : MonoBehaviour
    {
        [SerializeField] private GameActorsPool pool;
        [SerializeField] private string missileGameActorId;
        [SerializeField] private float missileCreationDistance = 1;
        [SerializeField] private float launchesTimeInterval = .3f;
        [SerializeField] private float directionRotationSpeed = 1;

        private float t;
        
        private WaitForSeconds waiter;


        private void Awake()
        {
            waiter = new WaitForSeconds(launchesTimeInterval);
        }

        private void OnEnable()
        {
            StopAllCoroutines();
            StartCoroutine(LaunchMissileAfterDelay());
            t = Random.Range(0, Mathf.PI * 2);
        }

        private void OnDisable()
        {
            StopAllCoroutines();
        }

        private void Update()
        {
            t += Time.deltaTime * directionRotationSpeed;
        }

        private IEnumerator LaunchMissileAfterDelay()
        {
            yield return waiter;
            LaunchMissile();
            StartCoroutine(LaunchMissileAfterDelay());
        }
        
        public void LaunchMissile()
        {
            Vector3 pos = CalculateMissileSpawnPosition();
            GameActor missile = pool.GetGameActor(missileGameActorId, pos);
            missile.GetComponent<ContinuousMovement>().Direction = pos - transform.position;
        }

        Vector3 CalculateMissileSpawnPosition()
        {
            // For finding a point on a circle :
            // float x = r*cos(t) + h;
            // float y = r*sin(t) + k;
            // Where the center of the circle is (h, k), and the radius is r
            float x = missileCreationDistance * Mathf.Sin(t) + transform.position.x;
            float z = missileCreationDistance * Mathf.Cos(t) + transform.position.z;
            return new Vector3(x, 0, z);
        }
    }
}
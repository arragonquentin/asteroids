using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace QuentinArragon.AsteroidsGame
{
    public class OnAllGameActorsDeadEvent : MonoBehaviour
    {
        [SerializeField] private List<string> targetedActorTypeIds = new List<string>();
        [SerializeField] private GameActorsCollection gameActorsCollection;
        [SerializeField] private UnityEvent onAllDead = new UnityEvent();
        
        private void OnEnable()
        {
            gameActorsCollection.OnRemove += OnGameActorRemoved;
        }
        private void OnDisable()
        {
            gameActorsCollection.OnRemove -= OnGameActorRemoved;
        }

        private void OnGameActorRemoved(GameActor gameActor)
        {
            int count = 0;
            
            foreach (GameActor actor in gameActorsCollection)
            {
                // The game actor that has just been removed from the collection does not count
                if(actor == gameActor)
                    continue;
                if (targetedActorTypeIds.Contains(actor.ActorTypeId))
                    count++;
            }
            
            if(count == 0)
            {
                print($"On All Game Actors Dead - Invoke event. Collection count : {gameActorsCollection.Count}");
                //Debug.Break();
                onAllDead.Invoke();
            }
        }
    }
}